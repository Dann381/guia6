import java.util.HashSet;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
		//Creación del HashSet Estudiantes
		HashSet<String> Estudiantes = new HashSet<>();
		Estudiantes.add("Daniel Tobar");
		Estudiantes.add("Shoto Todoroki");
		Estudiantes.add("Pedro Tobar");
		Estudiantes.add("Florencia Tobar");
		Estudiantes.add("Karen Hormazábal");
		Estudiantes.add("Luis Tobar");
		Estudiantes.add("Mía Tobar");
		Estudiantes.add("Juanito Perez");
		Estudiantes.add("Will Smith");
		Estudiantes.add("Momo Yaoyorozu");
		Estudiantes.add("Mina Ashido");
		Estudiantes.add("Jackie Chan");
		Estudiantes.add("Donnie Yen");

		//Creación del Iterador
		Iterator<String> iterate = Estudiantes.iterator();
		
		System.out.println("Estudiantes: ");
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		
		//Consultamos si el HashSet contiene al Estudiante Donnie Yen
		System.out.println("\n¿Está el estudiante Donnie Yen en el HashSet Estudiantes?: " + Estudiantes.contains("Donnie Yen"));
		
		//Creación del segundo HashSet
		HashSet<String> Estudiantes2020 = new HashSet<>();
		Estudiantes2020.add("Donnie Yen");
		Estudiantes2020.add("Eren Jaeger");
		Estudiantes2020.add("Shoto Todoroki");
		Estudiantes2020.add("Jackie Chan");
		Estudiantes2020.add("Daniel Tobar");
		Estudiantes2020.add("Baki Hanma");
		Estudiantes2020.add("Momo Yaoyorozu");
		Estudiantes2020.add("Mina Ashido");
		Estudiantes2020.add("Mía Tobar");
		
		//Consultamos si el HashSet Estudiantes contiene a todos los elementos del HashSet Estudiantes2020
		System.out.println("¿Está Estudiantes2020 contenido en Estudiantes?: " + Estudiantes.containsAll(Estudiantes2020));
		
		//Unimos ambos conjuntos
		Estudiantes.addAll(Estudiantes2020);
		iterate = Estudiantes.iterator();
		System.out.println("\nUnión de ambos conjuntos: ");
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}

		//Removemos todos los elementos del HashSet Estudiantes que estén en el HashSet2020
		Estudiantes2020.removeAll(Estudiantes);
		System.out.println("\nElementos del conjunto Estudiantes2020 que no estén en el conjunto Estudiantes:\n" + Estudiantes2020);
		//Como el conjunto Estudiantes contiene todos los elementos del conjunto Estudiantes2020, al remover
		//los elementos el conjunto queda vacío
	}	
}
