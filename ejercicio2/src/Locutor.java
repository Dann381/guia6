import java.util.HashSet;

public class Locutor {
	HashSet<Integer> Numeros = new HashSet<>();
	Locutor(){
	}
	public void mostrarNumeros() {
		System.out.println(this.Numeros);
	}
	public HashSet<Integer> getNumeros(){
		return this.Numeros;
	}
	public void sacarBola() {
		Boolean bucle = false;
		while (bucle == false) {
			int random = (int) (Math.random()*30+1);
			Boolean var = Numeros.contains(random);
			if (var == true) {
				bucle = false;
			}
			else {
				this.Numeros.add(random);
				bucle = true;
				System.out.println("Se sacó el número " + random);
			}
		}
	}
}
