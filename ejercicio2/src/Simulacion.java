import java.util.HashSet;

public class Simulacion {
	
	
	Simulacion(){
		
		
		//Título
		System.out.println("<<<<<<<<<<<< Bingo de 3 jugadores >>>>>>>>>>>>\n");
		
		//Se crean los 3 jugadores
		Jugador jugador1 = new Jugador();
		System.out.println("Carton jugador 1");
		jugador1.mostrarCarton();
		
		Jugador jugador2 = new Jugador();
		System.out.println("Carton jugador 2");
		jugador2.mostrarCarton();
		
		Jugador jugador3 = new Jugador();
		System.out.println("Carton jugador 3");
		jugador3.mostrarCarton();
		
		//Se crea el locutor
		Locutor locutor = new Locutor();
		
		//Obtenemos los HashSet para poder manipularlos
		HashSet<Integer> Carton1 = jugador1.getCarton();
		HashSet<Integer> Carton2 = jugador2.getCarton();
		HashSet<Integer> Carton3 = jugador3.getCarton();
		HashSet<Integer> Numeros = locutor.getNumeros();
		
		//Variables para controlar la partida
		Boolean ganar = false;
		int contador = 1;
		
		System.out.println("\nIniciando partida...\n");
		
		while (ganar == false) {
			System.out.print("-Step " + contador + "- ");
			locutor.sacarBola();
			contador = contador + 1;
		
			//Removemos los números que sacó el locutor
			Carton1.removeAll(Numeros);
			Carton2.removeAll(Numeros);
			Carton3.removeAll(Numeros);
			
			//Actualizamos los HashSets
			jugador1.setCarton(Carton1);
			jugador2.setCarton(Carton2);
			jugador3.setCarton(Carton3);
		
			//Obtenemos el tamaño de cada HashSet
			int size1 = Carton1.size();
			int size2 = Carton2.size();
			int size3 = Carton3.size();
		
			//Evaluamos el tamaño de cada HashSet para obtener un ganador y terminar la partida
			if (size1 == 0) {
				System.out.println("\nGanador: jugador 1\n");
				ganar = true;
			}
			else if (size2 == 0) {
				System.out.println("\nGanador: jugador 2\n");
				ganar = true;
			}
			else if (size3 == 0) {
				System.out.println("\nGanador: jugador 3\n");
				ganar = true;
			}
			else {
				;
			}
		}
		System.out.println("Mostrando cartones finales: \n");
		System.out.println("Cartón jugador 1");
		jugador1.mostrarCarton();
		System.out.println("Cartón jugador 2");
		jugador2.mostrarCarton();
		System.out.println("Cartón jugador 3");
		jugador3.mostrarCarton();
	}
}
