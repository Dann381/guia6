import java.util.HashSet;

public class Jugador {
	//Creación del cartón del Jugador
	HashSet<Integer> Carton = new HashSet<>();
	Jugador(){
		int random;
		while (Carton.size() < 6) {
			random = (int)(Math.random()*30+1);
			Carton.add(random);
		}
	}
	public void mostrarCarton() {
		System.out.println(Carton);
	}
	public HashSet<Integer> getCarton() {
		return this.Carton;
	}
	public void setCarton(HashSet<Integer> Carton) {
		this.Carton = Carton;
	}
}
